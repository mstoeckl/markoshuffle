/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package markoshuffle;

/**
 *
 * Misc class to put all constants in that we don't have a place for.
 *
 * @author msto
 */
public class Constants {

    /* The first few word IDs are allocated to parts of speech & other token forms */
    public static final int N_RESERVED_IDS = 256;
    /* Tokens to extend n-grams to start/end of sentence */
    public static final int T_HEAD = 4;
    public static final int T_TAIL = 5;
    /* Parts of speech are corpus-dependent; we can assign them slots 16+ */
}
