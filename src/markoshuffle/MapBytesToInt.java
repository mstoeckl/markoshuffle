/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package markoshuffle;

/**
 * Customized map from bytes to integers, since standard collections are way too
 * slow, and this lets us transparently speed stuff up.
 *
 * @author msto
 */
public class MapBytesToInt {

    private static int hash(byte[] k) {
        int h = 0;
        for (byte x : k) {
            h = 37 * h + x;
        }
        return h & -1;
    }

    private static boolean eq(byte[] a, byte[] b) {
        if (a.length != b.length) {
            return false;
        }
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }

    private static final class Node {

        int val;
        byte[] data;
        Node next;

        Node(byte[] b, int idx) {
            data = b;
            val = idx;
            next = null;
        }
    }

    private Node[] store;
    private int elements;

    public static final float branchFactor = 65;

    public MapBytesToInt() {
        elements = 0;
        store = new Node[1 << 5];
    }

    public void put(byte[] key, int val) {
        int h = hash(key);
        int slot = h & (store.length - 1);
        Node n = store[slot];
        while (n != null) {
            if (eq(n.data, key)) {
                n.val = val;
                return;
            }
            n = n.next;
        }
        Node k = new Node(key, val);
        k.next = store[slot];
        store[slot] = k;
        elements++;
        if (elements * branchFactor > store.length * 100) {
            expand();
        }
    }

    public int getOrDefault(byte[] key, int def) {
        int h = hash(key);
        int slot = h & (store.length - 1);
        Node n = store[slot];
        while (n != null) {
            if (eq(n.data, key)) {
                return n.val;
            }
            n = n.next;
        }
        return def;
    }

    private void expand() {
        Node[] old = store;
        store = new Node[old.length * 2];
        for (Node n : old) {
            while (n != null) {
                Node q = n;
                n = q.next;
                int h = hash(q.data);
                int slot = h & (store.length - 1);
                Node s = store[slot];
                q.next = s;
                store[slot] = q;
            }
        }
    }
}
