/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package markoshuffle;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

/**
 * Main class to run naive fitting algorithm
 *
 * @author msto
 */
public class Markoshuffle {

    private static final Random rand = new Random();

    /**
     * @param args the command line arguments, to be ignored
     */
    public static void main(String[] args) {
        NGramSource.NGramLoader ngloader = null;
        if (args.length == 0) {
            ngloader = NGramSource.getDefault();
        } else if (args.length == 1) {
            ngloader = NGramSource.selectByKey(args[0]);
        }
        if (ngloader == null) {
            System.out.println("Usage: java -jar markoshuffle.jar datacode");
            System.out.println("The available data sources are:");
            String[] opts = NGramSource.listOptions();
            for (String o : opts) {
                System.out.format("\t%s\n", o);
            }

            System.exit(3);
            return;
        }

        /* Read the desired task set, and create a shuffled version */
        String[][] gold = loadInput(System.getenv("GRAVEYARD") + "/../ta.output");
//        String[][] gold = loadInput(System.getenv("GRAVEYARD") + "/testbank.txt");
        String[][] inp = new String[gold.length][];
        for (int i = 0; i < gold.length; i++) {
            if (gold[i].length == 0) {
                throw new IllegalStateException("Empty sentence!");
            }
            String[] sent = gold[i].clone();
            Arrays.sort(sent);
            inp[i] = sent;
        }

        /* Create the basic datastructures governing the words/ngrams. */
        MapBytesToInt intLookup = new MapBytesToInt();
        List<byte[]> wordLookup = new ArrayList<>();
        NGramTable unigrams = new NGramTable(1);
        NGramTable bigrams = new NGramTable(2);
        NGramTable trigrams = new NGramTable(3);
        NGramTable quatrigrams = new NGramTable(4);

        long itime = System.currentTimeMillis();
        System.out.println(ngloader.describe());
        if (!ngloader.canLoad()) {
            croak("Files for corpus not present.");
            return;
        }
        ngloader.load(wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams);

        long ftime = System.currentTimeMillis();
        System.out.printf("Loading corpus took: %f s\n", (ftime - itime) / 1000.);

        String color = "";
        String decolor = "";
        if (System.console() != null) {
            color = "\u001b[93m";
            decolor = "\u001b[0m";
        }

        double[] scores = new double[inp.length];
        double[] randoscores = new double[inp.length];
        ArrayList<String> net_res = new ArrayList<>();
        ArrayList<String> net_rand = new ArrayList<>();
        ArrayList<String> net_gold = new ArrayList<>();
        for (int i = 0; i < inp.length; i++) {
            String[] rando = shuffledOf(gold[i]);
            String[] result = unshuffleOne(inp[i], intLookup, wordLookup, unigrams, bigrams, trigrams);
            String[] reference = gold[i];
            double score = Scoring.preferredScore(result, reference);
            scores[i] = score;
            double rscore = Scoring.preferredScore(rando, reference);
            randoscores[i] = rscore;
            String sent = String.join(" ", result);
            System.out.printf("%.3f | %s%s%s\n", score, color, sent, decolor);
            for (String s : rando) {
                net_rand.add(s);
            }
            for (String s : result) {
                net_res.add(s);
            }
            for (String s : gold[i]) {
                net_gold.add(s);
            }
        }
        System.out.printf("Mean scores:\n");
        System.out.printf("  Random: ");
        Scoring.printStatisticsOn(randoscores);
        System.out.printf("  These:  ");
        Scoring.printStatisticsOn(scores);
        double group_rand_score = Scoring.preferredScore(net_rand.toArray(new String[0]), net_gold.toArray(new String[0]));
        double group_result_score = Scoring.preferredScore(net_res.toArray(new String[0]), net_gold.toArray(new String[0]));
        System.out.printf("Whole document scores: Rand: %.3f Results: %.3f Ref: 1.000\n", group_rand_score, group_result_score);

        String[] missed = new String[idkSet.size()];
        missed = idkSet.toArray(missed);
        Arrays.sort(missed);

        System.out.printf("Could not identify: |");
        for (String s : missed) {
            System.out.printf(" %s | ", s);
        }
        System.out.println();

        System.out.println("==== NGram statistics ====");
        System.out.println(unigrams.describe());
        System.out.println(bigrams.describe());
        System.out.println(trigrams.describe());
        System.out.println(quatrigrams.describe());
    }

    private static String[] shuffledOf(String[] given) {
        int[] idx = new int[given.length];
        for (int j = 0; j < idx.length; j++) {
            idx[j] = j;
        }
        shuffle(idx);
        String[] rando = new String[given.length];
        for (int j = 0; j < idx.length; j++) {
            rando[j] = given[idx[j]];
        }
        return rando;
    }

    private static void shuffle(int[] a) {
        /* Ye olde shuffle */
        for (int i = a.length - 1; i >= 0; i--) {
            int j = rand.nextInt(i + 1);
            int t = a[j];
            a[j] = a[i];
            a[i] = t;
        }
    }

    public static float rateSolutionConditional(int[] given, int[] transTable, NGramTable bigrams, NGramTable trigrams) {
        /* Translate sentence and introduce padding */
        int N = given.length;
        int[] tw = new int[N + 2];
        tw[0] = Constants.T_HEAD;
        for (int i = 0; i < N; i++) {
            tw[i + 1] = transTable[given[i]];
        }
        tw[N + 1] = Constants.T_TAIL;

        /* Add log-conditional probability scores */
        float score = 0.0f;
        for (int i = 1; i < N + 1; i++) {
            /* Calculate conditional probability of a word given other set.
             * We add one to all scores, skewing slightly but maintaining rank */
            int pin = trigrams.getOrDefault(tw[i - 1], tw[i], tw[i + 1], 0) + 1;
            int base = pin;
            /* We skip the two words flanking position i */
            for (int j = 1; j < i - 1; j++) {
                base += trigrams.getOrDefault(tw[i - 1], tw[j], tw[i + 1], 0) + 1;
            }
            for (int j = i + 2; j < N + 1; j++) {
                base += trigrams.getOrDefault(tw[i - 1], tw[j], tw[i + 1], 0) + 1;
            }

            float condprob = (float) pin / (float) base;
            score += Math.log(condprob);
        }
        return score;
    }

    public static float rateSolutionSumSquared(int[] given, int[] transTable, NGramTable bigrams, NGramTable trigrams) {
        /* We assume all ngram tables used have a common basis
         * Scores are allocated by sum-squared frequency; higher=better.
         * They start at one for convienience*/
        float score = 1.0f;
        int N = given.length;
        /* Yes, conditional probability is better, but a bit more expensive/complicated
         * Also, note that all negative values (unknown words) are never in ngram tables. */
        for (int i = 1; i < N; i++) {
            int value = bigrams.getOrDefault(transTable[given[i - 1]], transTable[given[i]], 0);
            score += (float) value * value;
        }

        return score;
    }

    private static float rateSolution(int[] given, int[] transTable, NGramTable bigrams, NGramTable trigrams) {
        // Takes about twice as much time, but gives _way_ better results
        return rateSolutionConditional(given, transTable, bigrams, trigrams);
        // Faster, measurably worse
//        return rateSolutionSumSquared(given, transTable, bigrams, trigrams);
    }

    /**
     * Pick an item from a CDF. CDF Format is:
     *
     * [0, A, B, C, D] (items = 4)
     *
     * where A=0+P(0), B=A+P(1), ...
     *
     * @param cdf
     * @param items
     * @return
     */
    private static int selectFromCDF(int[] cdf, int items) {
        int idx = rand.nextInt(cdf[items]);
        for (int j = 0; j < items; j++) {
            if (idx < cdf[j + 1]) {
                return j;
            }
        }
        throw new IllegalStateException("CDF badly ordered");
    }

    private static final Set<String> idkSet = new TreeSet<>();

    private static String[] unshuffleOne(String[] given,
            MapBytesToInt intLookup, List<byte[]> wordLookup,
            NGramTable unigrams, NGramTable bigrams, NGramTable trigrams) {
        /* Step 1: create the mapping; */
        int N = given.length;
        /* List of integers indicating transTable index */
        int[] wordlist = new int[N];
        /* List of integers indicating a unique word's translation */
        int ntrans = 0;
        int[] transTable = new int[N];
        Arrays.fill(transTable, -16777216);
        /* List of transTable indices which could lead a sentence */
        int nleaders = 0;
        int[] leaders = new int[N];
        Arrays.fill(leaders, -13);// < killme
        /* List of transTable indices which could END a sentence */
        int ntrailers = 0;
        int[] trailers = new int[N];
        Arrays.fill(trailers, -12);// < killme
        /* Fill! */
        for (int i = 0; i < N; i++) {
            String s = given[i];
            int v = -1;
            for (int j = 0; j < i; j++) {
                if (given[j].equals(s)) {
                    v = wordlist[j];
                    break;
                }
            }
            if (v >= 0) {
                wordlist[i] = v;
                continue;
            }
            wordlist[i] = ntrans;
            byte[] sb = s.getBytes(StandardCharsets.US_ASCII);
            transTable[ntrans] = intLookup.getOrDefault(sb, -i - 1);
            if (transTable[ntrans] < 0) {
                idkSet.add(s);
            }
            if (isInitial(s)) {
                leaders[nleaders] = ntrans;
                nleaders++;
            }
            if (isTerminal(sb)) {
                trailers[ntrailers] = ntrans;
                ntrailers++;
            }
            ntrans++;
        }

        /* Step 2: Solve! :-)*/
        shuffle(wordlist);

        long pre = System.nanoTime();
        int[] selected;
        if (N <= 9) {
            /* only a subset of 362880 combinations get tried! */
            selected = solveBruteForce(N, nleaders, ntrailers, wordlist,
                    leaders, trailers, bigrams, trigrams, transTable);
        } else {
            selected = solveBigramTrees(N, nleaders, ntrailers, wordlist,
                    leaders, trailers, bigrams, trigrams, transTable);
        }
        long post = System.nanoTime();
//        System.out.printf("%.6f ms\n", (post - pre) / 1e6);

        /* Step 3: Reverse the mapping */
        String[] real = new String[N];
        for (int j = 0; j < N; j++) {
            int wordval = transTable[selected[j]];
            if (wordval < 0) {
                real[j] = given[-wordval - 1];
            } else {
                real[j] = new String(wordLookup.get(wordval - Constants.N_RESERVED_IDS), StandardCharsets.US_ASCII);
            }
        }
        return real;
    }

    private static <T> T croak(String s, Object... x) {
        System.err.printf(s + "\n", x);
        System.exit(1);
        return null;
    }

    public static void blockIndefinitely() {
        try {
            Thread.sleep(10000000);
        } catch (InterruptedException ex) {
            ex.hashCode();
        }
    }

    /**
     * Read a file like `ta.output` and return its contents as an array of
     * arrays
     *
     * @param fname
     * @return
     */
    private static String[][] loadInput(String fname) {
        ArrayList<String[]> sents = new ArrayList<>();
        try {
            List<String> l = Files.readAllLines(Paths.get(fname));
            ArrayList<String> substr = new ArrayList<>();
            for (String q : l) {
                if (q.contains("=====")) {
                    if (substr.size() > 0) {
                        String[] s = new String[substr.size()];
                        substr.toArray(s);
                        substr.clear();
                        sents.add(s);
                    }
                } else {
                    for (String word : q.split("\\s+")) {
                        if (!word.isEmpty()) {
                            substr.add(word);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            return croak("Err: reading shuffled input (%s) failed.", fname);
        }

        return sents.toArray(new String[0][]);
    }

    public static boolean isTerminal(byte[] s) {
        switch (s[s.length - 1]) {
            case '.':
            case '!':
            case '?':
                return true;
            case '"':
            case '\'':
            case ')':
                /* dealing with closer elements */
                return s.length <= 1 ? false : isTerminal(Arrays.copyOf(s, s.length - 1));
            default:
                return false;
        }
    }

    private static boolean isInitial(String s) {
        // warning: need to string quotes/parens.
        boolean leadCap = ('A' <= s.charAt(0) && s.charAt(0) <= 'Z');
        if (leadCap) {
            return true;
        }
        if (s.length() > 1 && ('(' == s.charAt(0) || '"' == s.charAt(0))) {
            return isInitial(s.substring(1));
        }
        // accept digits as leads.
        return '0' <= s.charAt(0) && s.charAt(0) <= '9';
    }

    private static int[] solveBruteForce(int N, int nleaders, int ntrailers, int[] wordlist, int[] leaders,
            int[] trailers, NGramTable bigrams, NGramTable trigrams, int[] transTable) {
        /* iterate through all word list iterations; filter by lead/trail words, & go */
        if (nleaders == 0 || ntrailers == 0) {
            /* first, sanity checks */
            throw new IllegalStateException("All word sets must have either leading/trailing words.");
        }
        /* Permutation iteration by some swapping method: Setup.
         * Modelled off http://stackoverflow.com/questions/2799078/permutation-algorithm-without-recursion-java*/

        int[] words = Arrays.copyOf(wordlist, N);
        int[] swappings = new int[N];
        for (int i = 0; i < swappings.length; i++) {
            swappings[i] = i;
        }
        int[] selected = null;
        float best = Float.NEGATIVE_INFINITY;

        while (true) {
            /* filter most versions out. Yes, wasteful, whatever */
            boolean proper_start = false;
            boolean proper_end = false;
            for (int j = 0; j < nleaders; j++) {
                if (leaders[j] == words[0]) {
                    proper_start = true;
                }
            }
            for (int j = 0; j < ntrailers; j++) {
                if (trailers[j] == words[N - 1]) {
                    proper_end = true;
                }
            }
            if (proper_start && proper_end) {
                // Test sentence
                float score = rateSolution(words, transTable, bigrams, trigrams);
                if (score >= best) {
//                    System.out.printf("%f %s\n", score, Arrays.toString(words));
                    best = score;
                    /* Copy `words` array, because it gets mutated */
                    selected = Arrays.copyOf(words, words.length);
                }
            }

            /* Generate next sentence, by mutating words */
            int i = N - 1;
            while (i >= 0 && swappings[i] == N - 1) {
                int tmp = words[i];
                words[i] = words[swappings[i]];
                words[swappings[i]] = tmp;
                swappings[i] = i;
                i--;
            }
            if (i < 0) {
                break;
            } else {
                int prev = swappings[i];
                int tmp = words[i];
                words[i] = words[prev];
                words[prev] = tmp;
                int next = prev + 1;
                swappings[i] = next;
                int tmp2 = words[i];
                words[i] = words[next];
                words[next] = tmp2;
            }
        }

        return selected;
    }

    /**
     * Driver routine for the naive ngram search.
     *
     * @param wordlist
     * @param leaders
     * @param trailers
     * @param bigrams
     * @param transTable
     * @return
     */
    private static int[] solveBigramTrees(int N, int nleaders, int ntrailers, int[] wordlist, int[] leaders, int[] trailers, NGramTable bigrams, NGramTable trigrams, int[] transTable) {
        //
        // Brute force, but good enough. (Also, need a better wordset datastructure)
        //
        int[] selected = null;
        float best = Float.NEGATIVE_INFINITY;
        int[] cm_weights = new int[N + 1];
        for (int run = 0; run < 1000; run++) {
            WordSet words = new WordSet(wordlist);
            /* Results are generated anew _every time_ */
            int[] result = new int[N];
            Arrays.fill(result, -11);// < killme

            /* Select leading/trailing words which are _not_ the same. Assumes
               there are any and enough.*/
            int vlead, vtrail;
            if (nleaders == 0 || ntrailers == 0) {
                throw new IllegalStateException("All word sets must have either leading/trailing words.");
            }
            do {
                vlead = leaders[rand.nextInt(nleaders)];
                vtrail = trailers[rand.nextInt(ntrailers)];
            } while (vlead == vtrail && words.countWord(vlead) < 2);

            /* Select a leading word (have to do it first...) */
            result[0] = words.takeWord(vlead);
            result[N - 1] = words.takeWord(vtrail);

            int mid = N / 2;

            /* Select new words based on the previous word */
            for (int i = 1; i < mid; i++) {
                int prev = result[i - 1];
                int pwd = transTable[prev];
                if (pwd < 0) {
                    /* Pick next word at random */
                    result[i] = words.takeIndex(rand.nextInt(words.left));
                } else {
                    // Pick weighted by condition frequencies of each, times
                    // the frequency with which it is left
                    cm_weights[0] = 0;
                    for (int j = 0; j < words.left; j++) {
                        int nwd = transTable[words.data[j]];
                        /* ensure some chance of picking any value */
                        int s = bigrams.getOrDefault(pwd, nwd, 1);;
                        cm_weights[j + 1] = cm_weights[j] + s;
                    }
                    int idx = selectFromCDF(cm_weights, words.left);
                    result[i] = words.takeIndex(idx);
                }
            }

            /* Select new words based on the following word
             * Yes, this is basically a carbon copy. */
            for (int i = N - 2; i >= mid; i--) {
                int nwd = transTable[result[i + 1]];
                if (nwd < 0) {
                    /* Pick next word at random */
                    result[i] = words.takeIndex(rand.nextInt(words.left));
                } else {
                    cm_weights[0] = 0;
                    for (int j = 0; j < words.left; j++) {
                        int pwd = transTable[words.data[j]];
                        /* ensure some chance of picking any value */
                        int s = bigrams.getOrDefault(pwd, nwd, 1);;
                        cm_weights[j + 1] = cm_weights[j] + s;
                    }
                    int idx = selectFromCDF(cm_weights, words.left);
                    result[i] = words.takeIndex(idx);
                }
            }

            /* Result should be complete */
            assert words.left == 0;
            /* Higher scores are better */
            float score = rateSolution(result, transTable, bigrams, trigrams);
            if (score > best) {
                selected = result;
                best = score;
            }
        }
        return selected;
    }
}
