/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package markoshuffle;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

/**
 * NGram sources/corpora/intermediates. Class exists to centralize NGram
 * loading, and make it easier to handle the ten+ possible corpora.
 *
 * @author msto
 */
public class NGramSource {

    private static final String corpusFolder = System.getenv("GRAVEYARD") + "/";

    private static final NGramLoader[] opts = {
        new CachedNGramSource(new TableNGramSource("coha", "Corpus of Historical American English (440M words)")),
        new TableNGramSource("coca", "Corpus of Contemporary American English, Top 1M (520M words)"),
        new CorpusNGramSource("oanc", "Open American National Corpus (15M words)"),
        new CorpusNGramSource("guten", "Cleaned Subset of Project Gutenberg (200M words)"),
        new CachedNGramSource(new CorpusNGramSource("newscom", "News Commentary corpus (3M words)")),
        new CorpusNGramSource("aclarc", "ACL Anthology Reference Corpus (40M words?)"),
        new CorpusNGramSource("bnc", "British National Corpus (100M words)"),
        new CachedNGramSource(new CorpusNGramSource("craft", "CRAFT Corpus (400K words)"))
    };
    private static final boolean[] legal = new boolean[256];

    static {
        Pattern good = Pattern.compile("[a-zA-Z0-9,;:/$%\\-\\.\\(\\)\\?\\!'\"]");
        for (int i = 0; i < 256; i++) {
            byte[] bbuf = {(byte) i};
            boolean ok = good.asPredicate().test(new String(bbuf));
            legal[i] = ok;
        }
    }

    private static <T> T croak(String s, Object... x) {
        System.err.printf(s + "\n", x);
        System.exit(1);
        return null;
    }

    public static NGramLoader getDefault() {
        return selectByKey("oanc");
    }

    /**
     * Meant for UI/command line.
     *
     * @return
     */
    public static String[] listOptions() {
        String[] s = new String[opts.length];
        for (int i = 0; i < opts.length; i++) {
            s[i] = opts[i].key() + "\t\t" + opts[i].describe();
        }
        Arrays.sort(s);
        return s;
    }

    public static NGramLoader selectByKey(String key) {
        for (NGramLoader l : opts) {
            if (l.key().equals(key)) {
                return l;
            }
        }
        return null;
    }

    private static InputStream corpusFileStream(String name) {
        String n = corpusFolder + name + ".gz";
        InputStream s;
        try {
            s = new GZIPInputStream(new FileInputStream(n), 1 << 16);
        } catch (IOException ex) {
            return croak("Could not open corpoid (%s)", n);
        }
        System.out.printf("Reading %s ...\n", n);
        return s;
    }

    private static void commentOnNGrams(List<byte[]> wordLookup, NGramTable unigrams, NGramTable bigrams,
            NGramTable trigrams, NGramTable quatrigrams) {
        System.out.printf("Unigram count: %d\n", unigrams.size());
        System.out.printf("Bigram count: %d\n", bigrams.size());
        System.out.printf("Trigram count: %d\n", trigrams.size());
        System.out.printf("Quatrigram count: %d\n", quatrigrams.size());
        System.out.printf("Total words: %d\n", wordLookup.size());
    }

    /**
     * I expect _strict_ formatting; then they load faster.
     *
     * WORD0\tWORD1\tWORD2..\n
     *
     * This method does not distinguish between ints/bytes; the byte[]->count
     * conversion is one you can do later. Yes, this thing spams temporaries.
     */
    private static void readTSVData(InputStream istream, MapBytesToInt intLookup, List<byte[]> wordLookup, NGramTable t) {
        int size = 1 << 20;
        byte[] buf = new byte[size];
        try {
            int upperlimit = istream.read(buf);
            int[] markers = new int[6];
            int[] codes = new int[4];
            Arrays.fill(codes, -1);
            int eol = -1;
            while (true) {
                // Scan ahead to newline; then pull in int, N words
                eol++;
                int fnd = 1;
                int sol = eol;
                markers[0] = sol;
                while (buf[eol] != '\n' && eol < upperlimit) {
                    if (buf[eol] == '\t') {
                        markers[fnd] = eol;
                        fnd++;
                    }
                    eol++;
                }
                markers[fnd] = eol;
                fnd++;
                if (eol >= upperlimit) {
                    // Copy tail, read more, retry
                    int leftover = upperlimit - sol;
                    System.arraycopy(buf, sol, buf, 0, leftover);
                    int n = istream.read(buf, leftover, buf.length - leftover);
                    if (n < 0) {
                        break;
                    }
                    upperlimit = n + leftover;
                    eol = -1;
                    continue;
                }

                // TODO: integer conversion microroutine
                int count = Integer.parseInt(new String(buf, markers[0], markers[1] - markers[0], StandardCharsets.US_ASCII));

                for (int i = 1; i < fnd - 1; i++) {
                    byte[] s = Arrays.copyOfRange(buf, markers[i] + 1, markers[i + 1]);
                    int k = intLookup.getOrDefault(s, -1);
                    if (k == -1) {
                        k = wordLookup.size() + Constants.N_RESERVED_IDS;
                        intLookup.put(s, k);
                        wordLookup.add(s);
                    }
                    codes[i - 1] = k;
                }
                // We assume it was empty before
                switch (t.getN()) {
                    case 1:
                        t.put(codes[0], count);
                        break;
                    case 2:
                        t.put(codes[0], codes[1], count);
                        break;
                    case 3:
                        t.put(codes[0], codes[1], codes[2], count);
                        break;
                    case 4:
                        t.put(codes[0], codes[1], codes[2], codes[3], count);
                        break;
                }
            }
        } catch (IOException ex) {
            croak("Hit in the IO. Ugh.");
        }
    }

    private static void countSentence(int[] a, NGramTable unigrams,
            NGramTable bigrams, NGramTable trigrams, NGramTable quatrigrams) {
        unigrams.increment(a[0]);
        if (a.length <= 1) {
            return;
        }
        unigrams.increment(a[1]);
        bigrams.increment(a[0], a[1]);
        if (a.length <= 2) {
            return;
        }
        unigrams.increment(a[2]);
        bigrams.increment(a[1], a[2]);
        trigrams.increment(a[0], a[1], a[2]);
        for (int i = 3; i < a.length; i++) {
            unigrams.increment(a[i]);
            bigrams.increment(a[i - 1], a[i]);
            trigrams.increment(a[i - 2], a[i - 1], a[i]);
            quatrigrams.increment(a[i - 3], a[i - 2], a[i - 1], a[i]);
        }
    }

    /**
     * Reads and simultaneously analyzes a corpus.
     *
     * (Why simultaneously? to minimize memory usage.)
     *
     * @param istream
     * @param wordLookup
     * @param intLookup
     */
    private static void fastMunchies(InputStream istream,
            List<byte[]> wordLookup, MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams, NGramTable trigrams, NGramTable quatrigrams) {
        int size = 1 << 20;
        byte[] buf = new byte[size];
        int maxsent = 1 << 16;
        int[] sent = new int[maxsent];
        int nwords = 0;
        int total_words = 0;
        int total_sentences = 0;
        try {
            int upperlimit = istream.read(buf);

            int markbot = 0;
            int marktop = 0;
            while (true) {
                // Scan the next word, refilling if necessary
                markbot = marktop;
                while ((!legal[buf[markbot] & 0xFF]) && markbot < upperlimit) {
                    markbot++;
                }
                if (markbot >= upperlimit) {
                    // Not inside a word, so just reload the full buffer
                    upperlimit = istream.read(buf);
                    if (upperlimit < 0) {
                        break;
                    }
                    marktop = 0;
                    continue;
                }
                marktop = markbot;
                while (legal[buf[marktop] & 0xFF] && marktop < upperlimit) {
                    marktop++;
                }
                if (marktop >= upperlimit) {
                    // Inside a word, so restart from the beginning
                    int leftover = upperlimit - markbot;
                    System.arraycopy(buf, markbot, buf, 0, leftover);
                    int n = istream.read(buf, leftover, buf.length - leftover);
                    if (n < 0) {
                        // famous last words
                        break;
                    }
                    upperlimit = n + leftover;
                    marktop = 0;
                    continue;
                }

                // Now that we have a string, load it!
                byte[] s = Arrays.copyOfRange(buf, markbot, marktop);

                int k = intLookup.getOrDefault(s, -1);
                if (k == -1) {
                    k = wordLookup.size() + Constants.N_RESERVED_IDS;
                    intLookup.put(s, k);
                    wordLookup.add(s);
                }
                sent[nwords] = k;
                nwords++;

                if (Markoshuffle.isTerminal(s) || nwords >= maxsent) {
                    /* Pad blob with start/end markers & analyze */
                    int[] sbpc = new int[nwords + 2];
                    sbpc[0] = Constants.T_HEAD;
                    System.arraycopy(sent, 0, sbpc, 1, nwords);
                    sbpc[nwords + 1] = Constants.T_TAIL;
                    /* Analyze sentence */
                    countSentence(sbpc, unigrams, bigrams, trigrams, quatrigrams);
                    total_words += nwords;
                    total_sentences += 1;
                    nwords = 0;
                }
            }
        } catch (IOException ex) {
            croak("Hit in the IO. Ugh.");
        }
        System.out.printf("Sentences: %d\n", total_sentences);
        System.out.printf("Total words: %d\n", total_words);
    }

    private static void dumpData(String key, List<byte[]> wordLookup,
            NGramTable unigrams, NGramTable bigrams, NGramTable trigrams,
            NGramTable quatrigrams) {
        if (!Files.exists(Paths.get(corpusFolder + "dump/"))) {
            try {
                Files.createDirectories(Paths.get(corpusFolder + "dump/"));
            } catch (IOException ex) {
                croak("Failed to create directory %s for cache files", corpusFolder + "dump/");
            }
        }
        try {
            OutputStream ostream = new FileOutputStream(corpusFolder + "dump/" + key + ".words");
            for (byte[] b : wordLookup) {
                ostream.write(b);
                ostream.write('\n');
            }
            ostream.close();
            NGramTable.writeToFile(new FileOutputStream(corpusFolder + "dump/" + key + "1.gz"), unigrams);
            NGramTable.writeToFile(new FileOutputStream(corpusFolder + "dump/" + key + "2.gz"), bigrams);
            NGramTable.writeToFile(new FileOutputStream(corpusFolder + "dump/" + key + "3.gz"), trigrams);
            NGramTable.writeToFile(new FileOutputStream(corpusFolder + "dump/" + key + "4.gz"), quatrigrams);
        } catch (IOException ex) {
            croak("Can't write. Oh no!");
        }

    }

    private static void undumpData(String key, List<byte[]> wordLookup,
            MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams,
            NGramTable trigrams, NGramTable quatrigrams) {
        try {
            // A sign you've been programming for too long at a stretch:
            // Rolling everything yourself, for the third time
            InputStream istream = new FileInputStream(corpusFolder + "dump/" + key + ".words");
            byte[] buf = new byte[1 << 16];
            int start;
            int end = 0;
            int limit = istream.read(buf, 0, buf.length);
            do {
                start = end;
                while (end < limit && buf[end] != '\n') {
                    end++;
                }
                if (end >= limit) {
                    int extra = limit - start;
                    System.arraycopy(buf, start, buf, 0, extra);
                    int n = istream.read(buf, extra, buf.length - extra);
                    if (n < 0) {
                        break;
                    }
                    limit = extra + n;
                    end = 0;
                    continue;
                }
                byte[] word = Arrays.copyOfRange(buf, start, end);
                intLookup.put(word, wordLookup.size() + Constants.N_RESERVED_IDS);
                wordLookup.add(word);
                end++;
            } while (true);

            NGramTable.fillFromFile(new FileInputStream(corpusFolder + "dump/" + key + "1.gz"), unigrams);
            NGramTable.fillFromFile(new FileInputStream(corpusFolder + "dump/" + key + "2.gz"), bigrams);
            NGramTable.fillFromFile(new FileInputStream(corpusFolder + "dump/" + key + "3.gz"), trigrams);
            NGramTable.fillFromFile(new FileInputStream(corpusFolder + "dump/" + key + "4.gz"), quatrigrams);
        } catch (IOException ex) {
            croak("Can't read. Oh no!");
        }
    }

    public static interface NGramLoader {

        String describe();

        String key();

        boolean canLoad();

        void load(List<byte[]> wordLookup,
                MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams,
                NGramTable trigrams, NGramTable quatrigrams);
    }

    private static class CorpusNGramSource implements NGramLoader {

        private final String key;
        private final String desc;

        public CorpusNGramSource(String key, String desc) {
            this.key = key;
            this.desc = desc;
        }

        @Override
        public String describe() {
            return "Corpus source: " + desc;
        }

        @Override
        public boolean canLoad() {
            boolean cex = Files.exists(Paths.get(corpusFolder + key + ".gz"));

            return cex;
        }

        @Override
        public void load(List<byte[]> wordLookup, MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams, NGramTable trigrams, NGramTable quatrigrams) {
            fastMunchies(corpusFileStream(key), wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams);
            commentOnNGrams(wordLookup, unigrams, bigrams, trigrams, quatrigrams);
        }

        @Override
        public String key() {
            return key;
        }
    }

    private static class TableNGramSource implements NGramLoader {

        private final String key;
        private final String desc;

        public TableNGramSource(String key, String desc) {
            this.key = key;
            this.desc = desc;
        }

        @Override
        public String describe() {
            return "Table source: " + desc;
        }

        @Override
        public boolean canLoad() {
            boolean uex = Files.exists(Paths.get(corpusFolder + key + "1.gz"));
            boolean bex = Files.exists(Paths.get(corpusFolder + key + "2.gz"));
            boolean tex = Files.exists(Paths.get(corpusFolder + key + "3.gz"));
            boolean qex = Files.exists(Paths.get(corpusFolder + key + "4.gz"));
            return uex && bex && tex && qex;
        }

        @Override
        public void load(List<byte[]> wordLookup, MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams, NGramTable trigrams, NGramTable quatrigrams) {
            // Note that words in one list need not appear in another.
            readTSVData(corpusFileStream(key + "1"), intLookup, wordLookup, unigrams);
            readTSVData(corpusFileStream(key + "2"), intLookup, wordLookup, bigrams);
            readTSVData(corpusFileStream(key + "3"), intLookup, wordLookup, trigrams);
            readTSVData(corpusFileStream(key + "4"), intLookup, wordLookup, quatrigrams);
            commentOnNGrams(wordLookup, unigrams, bigrams, trigrams, quatrigrams);
        }

        @Override
        public String key() {
            return key;
        }
    }

    private static class DumpNGramSource implements NGramLoader {

        private final String key;
        private final String desc;

        public DumpNGramSource(NGramLoader l) {
            this.key = l.key();
            this.desc = l.describe();
        }

        @Override
        public String describe() {
            return "Preprocessed " + desc;
        }

        @Override
        public boolean canLoad() {
            boolean wex = Files.exists(Paths.get(corpusFolder + "dump/" + key + ".words"));
            boolean uex = Files.exists(Paths.get(corpusFolder + "dump/" + key + "1.gz"));
            boolean bex = Files.exists(Paths.get(corpusFolder + "dump/" + key + "2.gz"));
            boolean tex = Files.exists(Paths.get(corpusFolder + "dump/" + key + "3.gz"));
            boolean qex = Files.exists(Paths.get(corpusFolder + "dump/" + key + "4.gz"));
            return wex && uex && bex && tex && qex;
        }

        @Override
        public void load(List<byte[]> wordLookup, MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams, NGramTable trigrams, NGramTable quatrigrams) {
            // Note that words in one list need not appear in another.
            undumpData(key, wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams);
            commentOnNGrams(wordLookup, unigrams, bigrams, trigrams, quatrigrams);
        }

        @Override
        public String key() {
            return key;
        }
    }

    private static class CachedNGramSource implements NGramLoader {

        private NGramLoader primary;
        private NGramLoader cache;

        CachedNGramSource(NGramLoader given) {
            primary = given;
            cache = new DumpNGramSource(given);
        }

        @Override
        public String describe() {
            return "Cached " + primary.describe();
        }

        @Override
        public String key() {
            return primary.key();
        }

        @Override
        public boolean canLoad() {
            return cache.canLoad() || primary.canLoad();
        }

        @Override
        public void load(List<byte[]> wordLookup, MapBytesToInt intLookup, NGramTable unigrams, NGramTable bigrams, NGramTable trigrams, NGramTable quatrigrams) {
            if (cache.canLoad()) {
                cache.load(wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams);
            } else {
                primary.load(wordLookup, intLookup, unigrams, bigrams, trigrams, quatrigrams);
                dumpData(primary.key(), wordLookup, unigrams, bigrams, trigrams, quatrigrams);
            }
        }
    }
}
