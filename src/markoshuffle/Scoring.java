/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package markoshuffle;

import java.util.HashMap;

/**
 *
 * Compare two sentences (taken as arrays of words) and determine how well they
 * match. It is already assumed that all contain the set with multiplicities of
 * words. Scores are all normalized on the range [0..1].
 *
 * @author msto
 */
public class Scoring {

    public static double preferredScore(String[] given, String[] gold) {
//        return editDistanceScore(given, gold);
        return bleuScore(given, gold, false);
    }

    /**
     * A simple and horribly crude metric of correctness.
     *
     * @param given Provided sentence.
     * @param gold Sentence to test against.
     * @return
     */
    public static double errFracScore(String[] given, String[] gold) {
        if (gold.length != given.length) {
            throw new IllegalStateException("Should only compare sentences of equal length");
        }
        int hits = 0;
        for (int i = 0; i < gold.length; i++) {
            if (gold[i].equals(given[i])) {
                hits++;
            }
        }
        return (double) hits / (double) gold.length;
    }

    /**
     * Score the sentence by one measure of edit distance from the reference.
     *
     * @param given Provided sentence.
     * @param gold Sentence to test against.
     * @return
     */
    public static double editDistanceScore(String[] given, String[] gold) {
        if (gold.length != given.length) {
            throw new IllegalStateException("Should only compare sentences of equal length");
        }

        int maxscore;
        int n2 = (given.length / 2);
        if (given.length % 2 == 1) {
            // e.g. N=9: 8+8+6+6+4+4+2+2
            maxscore = 2 * n2 * (n2 + 1);
        } else {
            // e.g. N=8: 7+7+5+5+3+3+1+1
            maxscore = 2 * n2 * n2;
        }
        int distance = 0;
        for (int i = 0; i < given.length; i++) {
            int mind = given.length;
            for (int j = 0; j < gold.length; j++) {
                if (gold[j].equals(given[i]) && Math.abs(i - j) < mind) {
                    mind = Math.abs(i - j);
                }
            }
            distance += mind;
        }
        return 1.0 - (double) distance / (double) maxscore;
    }

    private static double kGramScore(String[] given, String[] gold, int k) {
        if (k <= 0) {
            throw new IllegalStateException("Can't have 0 or less-grams");
        }
        if (k > given.length) {
            return 1.0;
        }
        HashMap<String, Integer> givencounts = new HashMap<>();
        HashMap<String, Integer> goldcounts = new HashMap<>();
        for (int i = 0; i <= given.length - k; i++) {
            String root = given[i];
            for (int j = 1; j < k; j++) {
                root += "_====_" + given[i + j];
            }
            givencounts.put(root, givencounts.getOrDefault(root, 0) + 1);
        }
        for (int i = 0; i <= gold.length - k; i++) {
            String root = gold[i];
            for (int j = 1; j < k; j++) {
                root += "_====_" + gold[i + j];
            }
            goldcounts.put(root, goldcounts.getOrDefault(root, 0) + 1);
        }
        /* Quote: BLEU: a Method for Automatic Evaluation of Machine Translation
         *
         * Modified n-gram precision is computed similarly
         * for any n: all candidate n-gram counts and their
         * corresponding maximum reference counts are col-
         * lected. The candidate counts are clipped by their
         * corresponding reference maximum value, summed,
         * and divided by the total number of candidate n-
         * grams.
         */
        int tcnt = 0;
        for (String s : givencounts.keySet()) {
            int refcnt = goldcounts.getOrDefault(s, 0);
            int mycnt = givencounts.getOrDefault(s, 0);
            int clipcnt = Math.min(mycnt, refcnt);
            tcnt += clipcnt;
        }
        int nngrams = gold.length + 1 - k; // Would be different had we 2+ references
        double score = (double) tcnt / (double) nngrams;
        return score;
    }

    /**
     * Unsmoothed BLEU score (totally got this one wrong, whatever).
     *
     * @param given
     * @param gold
     * @return
     */
    public static double bleuScore(String[] given, String[] gold, boolean smooth) {
        /* Smoothing as per mteval-v13.pl */
        double[] weights = {0.25, 0.25, 0.25, 0.25}; // Must sum to 1
        double[] ngram_scores = new double[weights.length];
        for (int j = 0; j < ngram_scores.length; j++) {
            ngram_scores[j] = kGramScore(given, gold, j + 1);
        }

        // We do not bother with the brevity penalty, as given/gold lengths are equal
        // Computed weighted geometric mean
        double lgscore = 0;
        int onset = -1;
        for (int j = 0; j < weights.length; j++) {
            if (smooth && onset < 0 && ngram_scores[j] <= 0.0) {
                onset = j;
            }
            double smoothscore;
            if (smooth && ngram_scores[j] <= 0.0) {
                if (j < given.length) {
                    smoothscore = Math.pow(0.5, j - onset + 1) / (given.length - j);
                } else {
                    smoothscore = 1.0;
                }
            } else {
                smoothscore = ngram_scores[j];
            }
            lgscore += weights[j] * Math.log(smoothscore);
        }
        double score = Math.exp(lgscore);

//        System.out.println();
//        System.out.println(Arrays.toString(given));
//        System.out.println(Arrays.toString(gold));
//        System.out.printf("%s -> %f\n", Arrays.toString(ngram_scores), score);
        return score;

    }

    static void printStatisticsOn(double[] scores) {
        double net = 0.;
        double mom = 0.;
        for (int i = 0; i < scores.length; i++) {
            net += scores[i];
            mom += scores[i] * scores[i];
        }
        double mean = net / scores.length;
        double mom2 = mom / scores.length;
        double stdev = Math.sqrt((mom2 - mean * mean) / (scores.length - 1));
        System.out.printf("Average translation score: %.3f +/- %.3f\n", mean, stdev);
    }
}
