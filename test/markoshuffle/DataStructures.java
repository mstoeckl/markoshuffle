/*
 * Copyright 2016 by the House Stark contributors
 * SPDX-License-Identifier: CC0-1.0
 */
package markoshuffle;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author msto
 */
public class DataStructures {

    public DataStructures() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    @Test
    public void valueInValueOut() {
        NGramTable t = new NGramTable(3);
        t.put(55, 67, 128, 11);
        int r = t.getOrDefault(55, 67, 128, 0);
        assertEquals(r, 11);
    }

    @Test
    public void ngramAddition() {
        NGramTable t = new NGramTable(4);
        t.increment(55, 67, 128, 44);
        t.increment(44, 67, 120, 44);
        t.increment(55, 67, 128, 44);
        int r = t.getOrDefault(55, 67, 128, 44, 0);
        assertEquals(r, 2);
    }

    @Test
    public void speedTest() {
        Random rando = new Random(672130870);
        NGramTable db = new NGramTable(2);
        /* Million entries, million squares, should give some (not many) gaps */
        int ntrials = 100000;
        int basis = 1 << 30;
        int[] opts = new int[2 * ntrials];
        for (int i = 0; i < ntrials; i++) {
            int x = rando.nextInt(basis);
            int y = rando.nextInt(basis);
            opts[2 * i] = rando.nextInt(basis);
            opts[2 * i + 1] = rando.nextInt(basis);
            db.increment(x, y);
        }
        for (int j = 0; j < 10; j++) {
            long count = 0;
            long pretime = System.nanoTime();
            for (int i = 0; i < ntrials; i++) {
                count += db.getOrDefault(opts[2 * i + 1], opts[2 * i], 0);
                db.increment(opts[2 * i + 1], opts[2 * i]);
            }
            long postTime = System.nanoTime();
            System.out.printf("compu %d\n", count);
            System.out.printf("delta %f ns/think\n", (postTime - pretime) / (double) ntrials);
        }
        System.out.println(db.describe());
    }

    @Test
    public void ngramRoundTrip() {
        Random rando = new Random(63213123);
        NGramTable from = new NGramTable(2);
        final int dim = 50;
        int[][] values = new int[dim][dim];
        for (int i = 0; i < 100000; i++) {
            int x = rando.nextInt(dim);
            int y = rando.nextInt(dim);
            from.increment(x, y);
            values[x][y] += 1;
        }
        NGramTable to = new NGramTable(2);
        ByteArrayOutputStream bo = new ByteArrayOutputStream(dim * dim * 64);
        NGramTable.writeToFile(bo, from);
        byte[] data = bo.toByteArray();
        ByteArrayInputStream bi = new ByteArrayInputStream(data);
        NGramTable.fillFromFile(bi, to);
        int[][] clone = new int[dim][dim];
        for (int x = 0; x < dim; x++) {
            for (int y = 0; y < dim; y++) {
                clone[x][y] = to.getOrDefault(x, y, 0);
            }
        }
        int[][] orig2 = new int[dim][dim];
        for (int x = 0; x < dim; x++) {
            for (int y = 0; y < dim; y++) {
                orig2[x][y] = from.getOrDefault(x, y, 0);
            }
        }
        Assert.assertArrayEquals("Bad place!", values, orig2);
        Assert.assertArrayEquals("Mismatch!", values, clone);
    }
}
